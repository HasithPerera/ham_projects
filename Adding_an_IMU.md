# Adding an IMU to the Sat Antenna on Yeasu 5500 (Summer 22)

This could improve the current tracking system and its easy to add-on with the k3ng_controller. 

1. going with a  **ADAFRUIT_LSM303** would be a suitable module

- [From adafruit](https://www.adafruit.com/product/4413) - Not available for now
- from Amazon : ~20$ with 2 mount holes slightly older board

2. mount 2 separate sensors
    - HMC5883L
    - ADXL345

Ether option is fine I prefer option one. even we go with option 2 the use the same i2c bus no change 

# Possible options for mounting 

1.  Mount it on a outdoor IP65 rated project box with an ether net cable going inside. We can put a fancy outdoor RJ45 connector.
    - [option 1](https://www.amazon.com/CNLINKO-Industrial-Connector-Receptacles-Waterproof/dp/B07GH7KTSS/ref=sr_1_21?keywords=outdoor+ethernet+jack&qid=1651792571&sr=8-21) : will cost more for connectors but gives easy upgrade 
    - option 2 : seal a cat5 outdoor cable with some sort of water tight glue. will get the job done

## Things that need testing - to get it done 

- [ ] length of the etharnet cable run ------
- [ ] if i2c will work over the whole cable run 
- [ ] whats the maximum sampling rate that is supported over the expected length

## Questions 

1. need to read about calibration of the IMU
2. evaluate before and after 


Last updated : 05/05/22 KE8TJE
    
